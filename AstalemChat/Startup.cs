﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System.IO;
using Microsoft.Extensions.FileProviders;
using AstalemChat.Data;
using AstalemChat.Models;
using AstalemChat.Services;
using Microsoft.OpenApi.Models;

namespace AstalemChat   
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //  Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v2", new OpenApiInfo { Title = "My API", Version = "v2" });
            });






            services.AddCors();//signalr config


            // Ended Looping in api json
            services.AddMvc().AddXmlSerializerFormatters().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            /*************Password Configration *************/
            services.Configure<IdentityOptions>(options =>
            {

                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 1;
                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(60);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {

                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(120);
                // If the LoginPath isn't set, ASP.NET Core defaults
                // // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            /*************Password Configration *************/

            /*************Time Configration *************/

            services.Configure<SecurityStampValidatorOptions>(options => options.ValidationInterval = TimeSpan.FromSeconds(2000));
            services.AddAuthentication()
                .Services.ConfigureApplicationCookie(options =>
                {
                    options.SlidingExpiration = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(300);
                    //Detect Logout Time

                });
            /*************Time Configration *************/

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application XML services. 
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddMvc().AddXmlSerializerFormatters().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);


            //For Add Service To Convert int To string
            //services.AddScoped<ConvertService, ConvertService>();
            //services.AddMvc(option =>
            //{
            //    option.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            //});
            //For PDF
            //services.AddJsReport(new LocalReporting()
            //    .UseBinary(JsReportBinary.GetBinary())
            //    .AsUtility()
            //    .Create());
            //For PDF

            services.AddSignalR();//add signalr
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseCors();//signal r config


            // Enable middleware to serve generated Swagger as a JSON endpoint.

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.




            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", "My API V2");
            });






            if (!env.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");

            }
            else
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }

            app.UseStaticFiles();
            /********Static File************/
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Upload")),
                RequestPath = "/Upload"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images")),
                RequestPath = "/wwwroot/images"
            });
            /********Static File************/
            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<Hubs.MainHub>("/chatHubs");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
              template: "{controller=Home}/{action=Index}/{id?}");
                //  template: "{controller=Account}/{action=Login}/{id?}");      

            });
            //  CreateUserRoles(serviceProvider);
        }
    }
}