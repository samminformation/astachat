﻿using AstalemChat.Data;
using AstalemChat.Models.ChatMode;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace Hubs
{

    public class MainHub : Hub
    {
        private readonly ApplicationDbContext _context;
        public MainHub(ApplicationDbContext Context)
        {
            _context = Context;
        }



        public async Task SendMessage([FromBody] Chat chat)
        {
            await Clients.All.SendAsync("ReceiveMessage", 1001);
        }


        public async Task SaveUserConnectionId(string userId)      
        {

            var user = await _context.ChatUser.FindAsync(userId);
            if (user != null)
            {
                user.ConnectionId = Context.ConnectionId;
                _context.Update(user);
                await _context.SaveChangesAsync();
            }else
            {
                ChatUser chatUser = new ChatUser();
                chatUser.Phone = userId;
                chatUser.ConnectionId = Context.ConnectionId;
                _context.Add(chatUser); 
                await _context.SaveChangesAsync();
            }
        }

    }

}
