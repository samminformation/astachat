﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AstalemChat.Models.ChatMode
{
    public class ChatUser
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Phone { get; set; }
        [Required]
        [Display(Name ="رقم الاتصال")]
        public string ConnectionId { get; set; }
    }
}
