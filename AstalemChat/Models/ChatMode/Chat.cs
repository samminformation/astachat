﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AstalemChat.Models.ChatMode
{
    public class Chat
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name ="الرسالة")]
        public string Message { get; set; }

        [Required]
        [Display(Name = "رسالتي")]
        public string UserPhone { get; set; }

        [Required]
        [Display(Name = "نوع الرسالة")]
        public string MessageType { get; set; } 

        [Required]
        [Display(Name = "رسالة غيري")]
        public string OtherUserPhone { get; set; }  

        [Required]
        [Display(Name = "الوقت")]
        public DateTime DateTimeNow { get; set; } 
    }
}