﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AstalemChat.Data;
using AstalemChat.Models.ChatMode;
using Hubs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using Zajel.Data;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using AstalemChat.Models;

namespace AstalemChat.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Chat/[Action]")]
    public class ChatController : Controller
    {

        private readonly ApplicationDbContext _context;
        private IHubContext<MainHub> _hubcontext { get; set; }
        public ChatController(ApplicationDbContext Context, IHubContext<MainHub> Hubcontext)
        {
            _context = Context;
            _hubcontext = Hubcontext;
        }
        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile image)
        {
            ResponseBody responseBody = new ResponseBody();
            responseBody.Message = await Upload.UploadImageAsync(image);
                return Ok(responseBody);
        }


        [HttpGet]
        public async Task<IActionResult> GetMyChat(string userPhone, string otherUserPhone) 
        {
            var chats = await _context.Chats.Where(m => m.UserPhone == userPhone && m.OtherUserPhone == otherUserPhone || m.UserPhone == otherUserPhone && m.OtherUserPhone == userPhone).ToListAsync();
                return Ok(chats.TakeLast(80));
        }







        [HttpPost]
        public async Task<IActionResult> SendMessage([FromBody]Chat chat)
        {
            ResponseBody responseBody = new ResponseBody();

            if (ModelState.IsValid)
            {
                var userChat =await _context.ChatUser.FindAsync(chat.OtherUserPhone);
                chat.DateTimeNow = DateTime.Now;

                await _hubcontext.Clients.Client(userChat.ConnectionId).SendAsync("ReceiveMessage", chat.Message,chat.OtherUserPhone, chat.DateTimeNow,chat.MessageType);
                
                _context.Add(chat);
                await _context.SaveChangesAsync();
                Console.WriteLine(await HttpGETPOST.GetToken(chat.OtherUserPhone)+"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                await Firebase.SendMessage(await HttpGETPOST.GetToken(chat.OtherUserPhone), "رسالة جديدة",chat.Message, chat.UserPhone, "chat");
                responseBody.Message = "1";
                return Ok(responseBody);   
            }
            else
            {
                responseBody.Message = "0";

                return Ok(responseBody);
            }
        } 


    }
}