﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

public class Firebase
{
    public static async Task SendMessage(string toUserToken, string title, string message, string phone, string type)
    {
        FireBaseData messages = new FireBaseData
        {
            to = toUserToken,
            title = title,
            body = message,
            phone = phone,
            type = type,
            content_available = true
        };


        FireBaseNotification notification = new FireBaseNotification
        {
            phone = phone,
            body = message,
            priority = "high",
            sound = "default",
            type = type,
            title = title,
            content_available = true
        };


        FireBaseMessage body = new FireBaseMessage()
        {
            to = toUserToken,
            data = messages,
            notification = notification
        };



        var client = new HttpClient();
        client.DefaultRequestHeaders.TryAddWithoutValidation(
            "Authorization", "Key=AAAABi7gZa4:APA91bEgCvscM_rN4blKsBdCbm3iUNAdgxFvWAGPJRyUKbs91cxNzyPfl_PEj-B2wcfEHckGCB4gFN49AG6i0QAyQEv5wSkox15ZfeWZAPH0OGPp04y5K7S6e5xIZX-Y0c1wYv4kbLEe");
        var result = new JsonContent(body);
        var response = await client.PostAsync("https://fcm.googleapis.com/fcm/send", result);
        var data2 = await response.Content.ReadAsStringAsync();

        Console.WriteLine($"data is : " + result.ToString());
    }
}

public class JsonContent : StringContent
{
    public JsonContent(object obj) :
        base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
    { }
}

public class FireBaseMessage
{
    public string to { get; set; }
    public FireBaseData data { get; set; }
    public FireBaseNotification notification { get; set; }
}

public class FireBaseNotification
{

    public string body { get; set; }
    public bool content_available { get; set; }
    public string sound { get; set; }
    public string priority { get; set; }
    public string type { get; set; }        
    public string title { get; set; }
    public string phone { get; set; }

}
public class FireBaseData
{
    public string title { get; set; }
    public string body { get; set; }
    public bool content_available { get; set; }
    public string phone { get; set; }
    public string type { get; set; }
    public string to { get; set; }

}
